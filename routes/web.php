<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
//index
Route::get('cats', function () {
    $cats = App\Cat::all();
    return view('cats.index')->with('cats', $cats);
});

//create
Route::get('cats/create', function () {
    return view('cats.create');
});

//store
Route::post('cats', function () {
    $cat = App\Cat::create(\Input::all());
    return redirect('cats/' . $cat->id)
        ->withSuccess('Cat has been created.');
});

//show
Route::get('cats/{id}', function ($id) {
    $cat = App\Cat::find($id);
    return view('cats.show')->with('cat', $cat);
});

// or Route-model binding
//Route::get('cats/{cat}', function(App\Cat $cat) {
//   return view('cats.show')->with('cat', $cat);
//});

//edit
Route::get('cats/{cat}/edit', function (App\Cat $cat) {
    return view('cats.edit')->with('cat', $cat);
});

//update
Route::put('cats/{cat}', function (App\Cat $cat) {
    $cat->update(Input::all());
    return redirect('cats/' . $cat->id)
        ->withSuccess('Cat has been updated.');
});

//destroy
Route::delete('cats/{cat}', function (App\Cat $cat) {
    $cat->delete();
    return redirect('cats')
        ->withSuccess('Cat has been deleted.');
});
*/
//Breeds
Route::get('cats/breeds/{name}', function ($name) {
    $breed = App\Breed::with('cats')
        ->whereName($name)
        ->first();
    return view('cats.index')
        ->with('breed', $breed)
        ->with('cats', $breed->cats);
});
//or use Resource controllers
Route::resource('cats', 'CatController');